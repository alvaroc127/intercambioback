/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Negocio;

import com.google.gson.Gson;
import com.intercambio.Enumeraciones.EEstadoGenerico;
import com.intercambio.Enumeraciones.Enumeraciones;
import com.intercambio.Persistencia.Articulo;
import com.intercambio.Persistencia.Cliente;
import com.intercambio.Persistencia.Solicitud;
import com.intercambio.Persistencia.Usuario;
import com.intercambio.Persistencia.dao.RepoInterfaceArticulo;
import com.intercambio.Persistencia.dao.RepoInterfaceSolicitud;
import com.intercambio.Persistencia.dao.RepoInterfaceCliente;
import com.intercambio.Persistencia.dao.RepoInterfaceUsuario;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.persistence.PersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



/**
 *
 * @author familia
 */
@Service
public class ServicioIntercambio {
  
  @Autowired
  RepoInterfaceArticulo articuloDAO;
  
  @Autowired
  RepoInterfaceSolicitud solicitudDAO;
  
  @Autowired
  RepoInterfaceCliente clienteDAO;
  
  @Autowired
  RepoInterfaceUsuario usuarioDAO;
  
  
 
  
  /**
   * Consulta Articulo 
   * @param articuloConsulta
   * @return 
   */
  @Transactional(readOnly = true) 
  public ResponseEntity consultarArticuloOtro(Articulo articuloConsulta)
  {
     Map<String,String> mapasalida=new HashMap();
    Gson gson=new Gson();
    if(null==articuloConsulta.getArNombre())
    {
      mapasalida.put(Enumeraciones.ERespuesta.ID, "-500");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "Sin nombre no se consulta");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
     return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(mapasalida));
    }
    Articulo articuloConsultado=articuloDAO.findByArNombreIgnoreCase(articuloConsulta.getArNombre());
    if(validarArticulo(articuloConsultado))
    {
      mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE," articulo encontrado ");
      articuloConsultado.setCliIdCliente(null);
      mapasalida.put(Enumeraciones.ERespuesta.DATA,gson.toJson(articuloConsultado));
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
    
    }
      mapasalida.put(Enumeraciones.ERespuesta.ID, "-200");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "NO se encontro el articulo");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
  }
  
  
  
  /**
   * Valida el articulo 
   * @param articulo
   * @return 
   */
  private boolean validarArticulo(Articulo articulo)
  {
    if(null != articulo)
    {
      boolean ban= null != articulo.getSoSolicitudRemitentes();
      boolean ban2=null != articulo.getSoSolicitudDestinatarios();   
      if(ban) articulo.getSoSolicitudRemitentes().clear();
      if(ban2) articulo.getSoSolicitudDestinatarios().clear(); 
      return true;
   }
   return false;
  }
  
  
  
  /**
   * Request for trade article
   * @param solicitud
   * @return 
   */
  public ResponseEntity solicitarIntercambio(Solicitud solicitud)
  {
     Map<String,String> mapasalida=new HashMap();
    Gson gson=new Gson();
    System.out.println(gson.toJson(solicitud));
    if(solicitud.validarSolicitud())
    {
      mapasalida.put(Enumeraciones.ERespuesta.ID, "-500");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, 
              "NO se pudo procesar la solicitud por favor verifique");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
    }
   solicitud.setSoCodigo(Enumeraciones.ESolicitud.INTERCAMBIO.getTipo()+
                         Enumeraciones.ESolicitud.INTERCAMBIO.getCodigo());
   solicitud.setSoEstado(EEstadoGenerico.ACTIVO);
   solicitud.setSoTipo(Enumeraciones.ESolicitud.INTERCAMBIO.getCodigo());
   try
   {
   solicitudDAO.save(solicitud);
      mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE," Solicitud procesada correctamente ");
      solicitud.setArIdArticuloDestinatario(null);
      solicitud.setArIdArticuloRemitente(null);
      mapasalida.put(Enumeraciones.ERespuesta.DATA,gson.toJson(solicitud));
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
   }catch(PersistenceException pes)
   {
     mapasalida.put(Enumeraciones.ERespuesta.ID,"-502");
     mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"No se puede procesar la solicitud BDD");
     mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
    return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
   }
  }
  
  
  @Transactional(readOnly = true)
  public  ResponseEntity consultarMisSolicitud(Usuario usuario)
  {
     Map<String,String> mapasalida=new HashMap();
    Gson gson=new Gson();
    System.out.println("se recibio el cliente");
    System.out.println(gson.toJson(usuario));
   try{
     if(null != usuario )
     {
       usuario=usuarioDAO.findByUsCorreo(usuario.getUsCorreo());
       Optional<Cliente> opcionCliente=clienteDAO.findById(usuario.getCliente().getCliIdregistro());
       List< Solicitud>solicitudes= solicitudDAO.findAllNative(opcionCliente.get().getCliIdregistro());
       if(null != solicitudes)
       {
         solicitudes.stream().
     forEach( soli -> {soli.getArIdArticuloDestinatario().setCliIdCliente(null); soli.getArIdArticuloRemitente().setCliIdCliente(null);});
     mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
     mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"Ok");
     mapasalida.put(Enumeraciones.ERespuesta.DATA,gson.toJson(solicitudes));
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
       }
     mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
     mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"NO se encontraron registros");
     mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
     }
     mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
     mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"Usuario o cliente vacio");
     mapasalida.put(Enumeraciones.ERespuesta.DATA, null);
     return ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
   }catch(PersistenceException pe){
     mapasalida.put(Enumeraciones.ERespuesta.ID, "-1");
     mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"ERROR EN LA CONSULTA");
     mapasalida.put(Enumeraciones.ERespuesta.DATA, null); 
     return ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
   }
  }
  
}
