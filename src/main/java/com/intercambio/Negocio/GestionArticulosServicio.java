/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Negocio;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.intercambio.Enumeraciones.EEstadoGenerico;
import com.intercambio.Enumeraciones.Enumeraciones;
import com.intercambio.Persistencia.Articulo;
import com.intercambio.Persistencia.Usuario;
import com.intercambio.Persistencia.dao.RepoInterfaceArticulo;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.PersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author familia
 */
@Service
public class GestionArticulosServicio {
  
  @Autowired
  RepoInterfaceArticulo articuloDAO;
  
  /**
   * Create or Update the product of user select
   * @param articulo
   * @return 
   */
  @Transactional(rollbackFor = Throwable.class)
  public ResponseEntity modificarArticuloGuardar(Articulo articulo)
  {
    Map<String,String> mapasalida=new HashMap();
    Gson gson=new Gson();
    boolean ban= null == articulo;
    if(ban)
    {
      mapasalida.put(Enumeraciones.ERespuesta.ID, "-501");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "Articulo no puede ser NULL");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
     return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(mapasalida));
    }
    try{
      articulo.setArEstado(EEstadoGenerico.ACTIVO);
    articuloDAO.save(articulo);
      mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "Base de datos actualizada");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
    }catch(PersistenceException pe)
    {
      pe.printStackTrace();
      mapasalida.put(Enumeraciones.ERespuesta.ID, "-501");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "Articulo no puede ser NULL");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
     return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(mapasalida));
    }
  }
  
  /**
   * Create or Update the product of user select
   * @return 
   */
  @Transactional(readOnly = true)
  public ResponseEntity consultarTipos()
  {
    Map<String,String> mapasalida=new HashMap();
    Gson gson=new Gson();
    try{
      mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "tipos");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,gson.toJson(Arrays.
           asList(Enumeraciones.EArticulo.values())));
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
    }catch(JsonParseException pe)
    {
      pe.printStackTrace();
      mapasalida.put(Enumeraciones.ERespuesta.ID, "-501");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "Error consultando los tipos");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
     return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(mapasalida));
    }
  }
  
  
  /**
   * Delete article
   * @param articulo
   * @return 
   */
  @Transactional(rollbackFor = Throwable.class)
  public ResponseEntity eliminarArticulo(Articulo articulo){
    Map<String,String> mapasalida=new HashMap();
    Gson gson=new Gson();
    boolean ban= null == articulo;
    if(ban)
    {
      mapasalida.put(Enumeraciones.ERespuesta.ID, "-501");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "Articulo no puede ser NULL");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
     return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(mapasalida));
    }
    try{
      articulo.setArEstado(EEstadoGenerico.ELIMINADO);
    articuloDAO.save(articulo);
      mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "Base de datos actualizada");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
    }catch(PersistenceException pe)
    {
      pe.printStackTrace();
      mapasalida.put(Enumeraciones.ERespuesta.ID, "-501");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "Articulo no se pudo eliminar");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
     return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(mapasalida));
    }
  }
  
  
  /**
   * 
   * @return 
   */
  @Transactional(readOnly = true)
  public ResponseEntity articulosDisponibles()
  {
     Map<String,String> mapasalida=new HashMap();
    Gson gson=new Gson();
    try{
      List<Articulo>  articulos
              =articuloDAO.findByArEstado(EEstadoGenerico.ACTIVO);
      
     if(null == articulos )
      {
      mapasalida.put(Enumeraciones.ERespuesta.ID, "-501");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "Sin articulos");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
      return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
      }
     if(articulos.isEmpty()){
       mapasalida.put(Enumeraciones.ERespuesta.ID, "-501");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "Sin articulos");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
      return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
     
     } 
     articulos.stream().
     forEach(
     artic -> 
     {artic.setSoSolicitudDestinatarios(null); 
     artic.setSoSolicitudRemitentes(null); 
     artic.setCliIdCliente(null);});
      mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "Base de datos actualizada");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,gson.toJson(articulos));
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
    }catch(PersistenceException pe)
    {
      pe.printStackTrace();
      mapasalida.put(Enumeraciones.ERespuesta.ID, "-501");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE, "Articulo no se pudo eliminar");
      mapasalida.put(Enumeraciones.ERespuesta.DATA,null);
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
    }
  }
  
  
}
