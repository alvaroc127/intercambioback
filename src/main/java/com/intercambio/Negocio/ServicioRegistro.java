/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Negocio;

import com.intercambio.Enumeraciones.Enumeraciones;
import com.intercambio.Persistencia.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.intercambio.Persistencia.dao.RepoInterfaceCliente;
import com.intercambio.Persistencia.dao.RepoInterfaceUsuario;
import com.google.gson.*;
import com.intercambio.Persistencia.Articulo;
import com.intercambio.Persistencia.Cliente;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.persistence.PersistenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author familia
 */
@Service
public class ServicioRegistro {
  
  @Autowired
  RepoInterfaceCliente clienteDAO;
  @Autowired
  RepoInterfaceUsuario usuarioDAO;
 
  
  
  @Transactional(rollbackFor = Throwable.class)
  public ResponseEntity RegistrarUsuario(Usuario usuario)
  {
    Map<String,String> mapasalida=new HashMap();
    Gson gson=new Gson();
   try{
     if(!validarExisteUsuario(usuario))
     {
      usuario.setUsPassword
      (ServicioCifrado.cifrado(usuario.getUsPassword()));
      Cliente clientenuevo=new Cliente();
      clientenuevo=clienteDAO.save(clientenuevo);
      usuario.setCliCliente(clientenuevo);
      usuarioDAO.save(usuario);
     mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
     mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"Ok");
     mapasalida.put(Enumeraciones.ERespuesta.DATA, null);
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
     }
     mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
     mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"Usuario  ya registrado");
     mapasalida.put(Enumeraciones.ERespuesta.DATA, null);
     return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(mapasalida));
   }catch(PersistenceException pe){
     mapasalida.put(Enumeraciones.ERespuesta.ID, "-1");
     mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"NO se pudo registrar");
     mapasalida.put(Enumeraciones.ERespuesta.DATA, null); 
     return ResponseEntity.status(HttpStatus.INSUFFICIENT_STORAGE).body(gson.toJson(mapasalida));
   }
  }
  
  
  
  /**
   * 
   * @param usuario
   * @return 
   */
  private boolean validarExisteUsuario(Usuario usuario)
  {
   return null!=usuarioDAO.findByUsCorreo(usuario.getUsCorreo())?true:false;
  }
  
  
  /**
   * validar el usuario enviado
   * @param usuario
   * @return 
   */
  @Transactional(readOnly = true)
  public ResponseEntity validarAutenticador(Usuario usuario){
    Map<String,String> mapasalida=new HashMap();
    Gson gson=new Gson();
    Usuario usuar=null;
    try{
      boolean ban=null != usuario && null != usuario.getUsCorreo() 
              && null != usuario.getUsPassword();
      if(ban)
      {
       usuar=usuarioDAO.findByUsCorreoAndUsPassword(usuario.getUsCorreo(),
              ServicioCifrado.cifrado(usuario.getUsPassword()));
      }
      if(!ban)
      {
        mapasalida.put(Enumeraciones.ERespuesta.ID, "-300");
        mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"Valide el usuario enviado");
       mapasalida.put(Enumeraciones.ERespuesta.DATA,gson.toJson(usuario)); 
      return ResponseEntity.badRequest().body(gson.toJson(mapasalida));
      }
      if(null != usuar)
      {
      usuar.getCliente().setArArticulos(null);
      mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"Bienvenido");
      mapasalida.put(Enumeraciones.ERespuesta.DATA, gson.toJson(usuar));
      return ResponseEntity.accepted().body(mapasalida);
      }
      mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
      mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"Usuario no registrado");
      mapasalida.put(Enumeraciones.ERespuesta.DATA, null);
     return ResponseEntity.accepted().body(mapasalida);
    }catch(PersistenceException pe)
    {
     mapasalida.put(Enumeraciones.ERespuesta.ID, "-100");
     mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"Error en la solicitud");
     mapasalida.put(Enumeraciones.ERespuesta.DATA, null); 
     return ResponseEntity.badRequest().body(gson.toJson(mapasalida));
    }
  }
  
  
   /**
   * Consultar mis articulos
   * @return 
   */
  @Transactional(readOnly = true)
  public  ResponseEntity consultarMisArticulos(Usuario usuario)
  {
     Map<String,String> mapasalida=new HashMap();
    Gson gson=new Gson();
    System.out.println("se recibio el cliente");
    System.out.println(gson.toJson(usuario.getCliente()));
   try{
     if(validarExisteUsuario(usuario))
     {
       usuario=usuarioDAO.findByUsCorreo(usuario.getUsCorreo());
     Optional<Cliente> opcionCliente=clienteDAO.findById(usuario.getCliente().getCliIdregistro());
     List< Articulo> articulos=opcionCliente.get().getArArticulos();
       System.out.println(articulos.toString());
             articulos.stream().
     forEach( artic -> {artic.setSoSolicitudDestinatarios(null); artic.setSoSolicitudRemitentes(null); artic.setCliIdCliente(null);});
             System.out.println(articulos.toString());
     mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
     mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"Ok");
     mapasalida.put(Enumeraciones.ERespuesta.DATA,gson.toJson(articulos));
     return  ResponseEntity.status(HttpStatus.OK).body(gson.toJson(mapasalida));
     }
     mapasalida.put(Enumeraciones.ERespuesta.ID, "200");
     mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"Usuario no registrado");
     mapasalida.put(Enumeraciones.ERespuesta.DATA, null);
     return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(mapasalida));
   }catch(PersistenceException pe){
     mapasalida.put(Enumeraciones.ERespuesta.ID, "-1");
     mapasalida.put(Enumeraciones.ERespuesta.MENSAJE,"ERROR EN LA CONSULTA");
     mapasalida.put(Enumeraciones.ERespuesta.DATA, null); 
     return ResponseEntity.status(HttpStatus.INSUFFICIENT_STORAGE).body(gson.toJson(mapasalida));
   }
  }
  
  
  
  
}
