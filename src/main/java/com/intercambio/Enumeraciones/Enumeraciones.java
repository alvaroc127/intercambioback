/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Enumeraciones;

/**
 *
 * @author familia
 */
public final class Enumeraciones {
  
    public enum ESolicitud{
    
    INTERCAMBIO("I",1);
    
    
    private final String tipo;
    private Integer codigo;

    private ESolicitud(String tipo, Integer codigo)
    {
      this.tipo = tipo;
      this.codigo = codigo;
    }

    
    public Integer getCodigo()
    {
    return codigo;
    }

   public void setCodigo(Integer codigo)
   {
    this.codigo = codigo;
   }

    public String getTipo()
    {
      return tipo;
    }
  
   
  }
    
   public enum EArticulo{
   
     INDUSTRIALES("I",1),
     HOGARES("H",2);
     
     private String tipo;
     private int codigo;

    private EArticulo(String tipo, int codigo)
    {
      this.tipo = tipo;
      this.codigo = codigo;
    }
    
    public String getTipo()
    {
      return tipo;
    }

    public void setTipo(String tipo)
    {
      this.tipo = tipo;
    }

    public int getCodigo()
    {
      return codigo;
    }

    public void setCodigo(int codigo)
    {
      this.codigo = codigo;
    }
   
   }
   
   public  final class ERuta{
   
     public static final String RUTA_PRUEBA1="/api/consulta/clientes/";
     public static final String REGISTRAR_USUARIO="/api/registro/usuario/";
     public static final String AUTENTICAR="/api/autenticar/user/";
     public static final String SOLICITAR_ARTICULO="/api/solicitar/articulo/";
     public static final String CONSULTAR_ARTICULO_OTRO="/api/intercambio/consultar/otro/";
     public static final String MODIFICAR_ARTICULO="/api/intercambio/modificar/otro/";
     public static final String CONSULTAR_TIPO_PRODUCTOS="/api/intercambio/consulta/tipoarticulo";
     public static final String ELIMINAR_ARTICULO="/api/intercambio/eliminar/articulo";
     public static final String CONSULTAR_MIS_ARTICULOS="/api/intercambio/consultar/misarticulos";
     public static final String CONSULTAR_MIS_SOLICITUDES="/api/intercambio/consutlar/missolicitudes";
     public static final String CONSULTAR_TODO_ARTICULOS="/api/intercambio/consutlar/articulos";
     public static final String CONSULTA_SOLICITUDES="/api/intercambio/consutlar/solicitudes";
     
   
   }
   
   public final class ERespuesta{
     public static final String ID="id";
     public static final String MENSAJE="mensaje";
     public static final String DATA="data";
   }
   
   
   
}