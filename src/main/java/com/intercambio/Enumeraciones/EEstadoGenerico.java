/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Enumeraciones;

/**
 *
 * @author familia
 */
public final class EEstadoGenerico {
  
  public static final String ACTIVO="A";
  public static final String ELIMINADO="E";
  public static final String PENDIENTE="P";
  public static final String RECHAZADO="R";
  public static final String INTERCAMBIO="I";
  
}
