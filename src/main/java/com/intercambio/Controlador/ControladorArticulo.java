/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Controlador;

import com.intercambio.Enumeraciones.Enumeraciones;
import com.intercambio.Negocio.GestionArticulosServicio;
import com.intercambio.Persistencia.Articulo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author familia
 */
@RestController
public class ControladorArticulo {
  
  @Autowired
  GestionArticulosServicio gestionServicio;
  
  /**
   * modifica un articulo 
   * @param articulo
   * @return 
   */
  @CrossOrigin(origins = "*",methods = {RequestMethod.POST})
  @PostMapping(Enumeraciones.ERuta.MODIFICAR_ARTICULO)
  public ResponseEntity modificarArticulo(@RequestBody Articulo articulo)
  {
    return gestionServicio.modificarArticuloGuardar(articulo);
  }
  
  
  @CrossOrigin( origins="*", methods = {RequestMethod.POST})
  @PostMapping(Enumeraciones.ERuta.CONSULTAR_TIPO_PRODUCTOS)
  public ResponseEntity consultarTipoProductos()
  {
    return gestionServicio.consultarTipos();
  }
  
  
  @CrossOrigin( origins="*", methods = {RequestMethod.POST})
  @PostMapping(Enumeraciones.ERuta.ELIMINAR_ARTICULO)
  public ResponseEntity eliminarArticulo(@RequestBody Articulo arituclo)
  {
    return gestionServicio.eliminarArticulo(arituclo);
  }
  

  /**
   * 
   * @return 
   */
  @CrossOrigin(origins = "*",methods = {RequestMethod.POST})
  @PostMapping(Enumeraciones.ERuta.CONSULTAR_TODO_ARTICULOS)
  public ResponseEntity consultarArticulos()
  {
    return gestionServicio.articulosDisponibles();
  
  }
  
  
}
