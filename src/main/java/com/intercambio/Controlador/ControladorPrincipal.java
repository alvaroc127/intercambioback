/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Controlador;

import com.intercambio.Enumeraciones.Enumeraciones;
import com.intercambio.Persistencia.Cliente;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.google.gson.*;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.intercambio.Persistencia.dao.RepoInterfaceCliente;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author familia
 */
@RestController
public class ControladorPrincipal {
  
  @Autowired
  RepoInterfaceCliente clientedao;
  
  @CrossOrigin(origins = "*",methods = {RequestMethod.POST,RequestMethod.GET})
  @RequestMapping( value="/", method = RequestMethod.GET)
  @ResponseBody
  public String inicioTranqui(){
  return "{\"inicio\":Tranqui}";
  
  }
  
   /**
   * 
   * @return 
   */
  @CrossOrigin(origins = "*",methods = {RequestMethod.POST,RequestMethod.GET})
  @RequestMapping(value=Enumeraciones.ERuta.RUTA_PRUEBA1 , method = RequestMethod.GET)
  @ResponseBody
  public String consultaClientes()
  {
    Gson gsonsalida=new Gson();
    List<Cliente> clientes=new ArrayList();
    clientedao.findAll().forEach( cliente->{
            cliente.setArArticulos(null);
           clientes.add(cliente);
                   }
    );
   return gsonsalida.toJson(clientes);
  }
 
  
  
 
  
}
