/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Controlador;

import com.intercambio.Enumeraciones.Enumeraciones;
import com.intercambio.Negocio.ServicioIntercambio;
import com.intercambio.Persistencia.Articulo;
import com.intercambio.Persistencia.Solicitud;
import com.intercambio.Persistencia.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author familia
 */

@RestController
public class ControladorIntercambio {
  
  
  @Autowired
  ServicioIntercambio servicioIntercambio;
  
  @CrossOrigin(origins = "*",methods = {RequestMethod.POST})
  @PostMapping(Enumeraciones.ERuta.CONSULTAR_ARTICULO_OTRO)
  public ResponseEntity consultarArticulo(@RequestBody Articulo articuloConsulta)
  {
    return servicioIntercambio.consultarArticuloOtro(articuloConsulta);
  }
  
  
  /**
   * does genered of request for the product
   * @param solicitud is request, with the content for the products to trade
   * @return 
   */
  @CrossOrigin(origins = "*",methods = {RequestMethod.POST})
  @PostMapping(Enumeraciones.ERuta.SOLICITAR_ARTICULO)
  public ResponseEntity solicitudIntercambio
        (@RequestBody Solicitud solicitud)
  {
    
    return servicioIntercambio.solicitarIntercambio(solicitud);
  
  }
  
 @CrossOrigin( origins="*", methods = {RequestMethod.POST})
  @PostMapping(Enumeraciones.ERuta.CONSULTA_SOLICITUDES)
  public ResponseEntity  consultaMisSolicitudes(@RequestBody Usuario usuario)
  {
    return servicioIntercambio.consultarMisSolicitud(usuario);
  }  
  
}
