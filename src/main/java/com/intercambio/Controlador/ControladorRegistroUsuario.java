/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Controlador;

import com.intercambio.Enumeraciones.Enumeraciones;
import com.intercambio.Negocio.ServicioRegistro;
import com.intercambio.Persistencia.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author familia
 */
@RestController
public class ControladorRegistroUsuario {
  
  @Autowired
  ServicioRegistro servicoRegistro;
  
  /**
   * Registrar usuaro
   * @param usuario 
   * @return 
   */
  @CrossOrigin(origins = "*",methods = {RequestMethod.POST})
  @PostMapping(Enumeraciones.ERuta.REGISTRAR_USUARIO)
  public ResponseEntity guardarUsuario(@RequestBody Usuario usuario)
  {
    
   return servicoRegistro.RegistrarUsuario(usuario);
  }
  
  /**
   * autenticador de usaurio 
   * @param usuario
   * @return 
   */
  @CrossOrigin(origins = "*",methods = {RequestMethod.POST})
  @PostMapping(Enumeraciones.ERuta.AUTENTICAR)
  public ResponseEntity autenticarUsuario(@RequestBody Usuario usuario)
  {
    return servicoRegistro.validarAutenticador(usuario);
  }
  
  @CrossOrigin( origins="*", methods = {RequestMethod.POST})
  @PostMapping(Enumeraciones.ERuta.CONSULTAR_MIS_ARTICULOS)
  public ResponseEntity  consultaMisArticulos(@RequestBody Usuario usuario)
  {
    return servicoRegistro.consultarMisArticulos(usuario);
  }  
}
