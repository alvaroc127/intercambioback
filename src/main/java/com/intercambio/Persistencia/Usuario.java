/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Persistencia;


import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author familia
 */
@Entity
@Table(name="usuario")
public class Usuario implements Serializable{
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer usIdregistro;
  private String usNombre;
  private String usPassword;
  private String usCorreo;
  private String usFoto;
  private String usEstado;
 
  @JoinColumn(name="cli_idecliente")
  @OneToOne
  private Cliente cliIdecliente;

  public Integer getUsIderegistro()
  {
    return usIdregistro;
  }

  public void setUsIderegistro(Integer usIderegistro)
  {
    this.usIdregistro = usIderegistro;
  }

  public String getUsNombre()
  {
    return usNombre;
  }

  public void setUsNombre(String usNombre)
  {
    this.usNombre = usNombre;
  }

  public String getUsPassword()
  {
    return usPassword;
  }

  public void setUsPassword(String usPassword)
  {
    this.usPassword = usPassword;
  }

  public String getUsCorreo()
  {
    return usCorreo;
  }

  public void setUsCorreo(String usCorreo)
  {
    this.usCorreo = usCorreo;
  }

  public String getUsFoto()
  {
    return usFoto;
  }

  public void setUsFoto(String usFoto)
  {
    this.usFoto = usFoto;
  }

  public String getUsEstado()
  {
    return usEstado;
  }

  public void setUsEstado(String usEstado)
  {
    this.usEstado = usEstado;
  }
  
  public Integer getCliCliente()
  {
     return cliIdecliente.getCliIdregistro();
  }
  
  public Cliente getCliente(){
    return cliIdecliente;
  }

  public void setCliCliente(Cliente cliCliente)
  {
    this.cliIdecliente = cliCliente;
  }
  
  
  
  
}
