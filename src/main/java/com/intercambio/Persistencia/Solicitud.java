/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Persistencia;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.ManyToAny;

/**
 *
 * @author familia
 */
@Entity
@Table(name="solicitud")
public class Solicitud {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer soIderegistro;
  private String soCodigo;
  private Integer soTipo;
  private String soEstado;
  private String soObservacion;
  
  @JoinColumn(name="ar_idarticuloremitente")
  @ManyToOne
  private Articulo arIdArticuloRemitente;
  
  @JoinColumn(name="ar_idarticulodestinatario")
  @ManyToOne
  private Articulo arIdArticuloDestinatario;

  
  public Integer getSoIderegistro()
  {
    return soIderegistro;
  }

  public void setSoIderegistro(Integer soIderegistro)
  {
    this.soIderegistro = soIderegistro;
  }

  public String getSoCodigo()
  {
    return soCodigo;
  }

  public void setSoCodigo(String soCodigo)
  {
    this.soCodigo = soCodigo;
  }

  public Integer getSoTipo()
  {
    return soTipo;
  }

  public void setSoTipo(Integer soTipo)
  {
    this.soTipo = soTipo;
  }

  public String getSoEstado()
  {
    return soEstado;
  }

  public void setSoEstado(String soEstado)
  {
    this.soEstado = soEstado;
  }

  public String getSoObservacion()
  {
    return soObservacion;
  }

  public void setSoObservacion(String soObservacion)
  {
    this.soObservacion = soObservacion;
  }

  public Articulo getArIdArticuloRemitente()
  {
    return arIdArticuloRemitente;
  }

  public void setArIdArticuloRemitente(Articulo arIdArticuloRemitente)
  {
    this.arIdArticuloRemitente = arIdArticuloRemitente;
  }

  public Articulo getArIdArticuloDestinatario()
  {
    return arIdArticuloDestinatario;
  }

  public void setArIdArticuloDestinatario(Articulo arIdArticuloDestinatario)
  {
    this.arIdArticuloDestinatario = arIdArticuloDestinatario;
  }
  
  public boolean validarSolicitud()
  {
    return null == this.arIdArticuloDestinatario &&
    null == this.arIdArticuloRemitente;
  }
  
  
  
}
