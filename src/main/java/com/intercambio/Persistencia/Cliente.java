/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Persistencia;


import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author familia
 */
@Entity
@Table(name="cliente")
public class Cliente implements Serializable {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer cliIdregistro;
  private String cliNombre;
  private String cliDireccion;
  private Integer cliTelefono;
  
  @OneToMany(mappedBy = "cliIdCliente")
  private List<Articulo> arArticulos;

  
  public Integer getCliIdregistro()
  {
    return cliIdregistro;
  }

  public void setCliIdregistro(Integer cliIdregistro)
  {
    this.cliIdregistro = cliIdregistro;
  }

  public List<Articulo> getArArticulos()
  {
    return arArticulos;
  }

  public void setArArticulos(List<Articulo> arArticulos)
  {
    this.arArticulos = arArticulos;
  }

  
  
  public String getCliNombre()
  {
    return cliNombre;
  }

  public void setCliNombre(String cliNombre)
  {
    this.cliNombre = cliNombre;
  }

  public String getCliDireccion()
  {
    return cliDireccion;
  }

  public void setCliDireccion(String cliDireccion)
  {
    this.cliDireccion = cliDireccion;
  }

  public Integer getCliTelefono()
  {
    return cliTelefono;
  }

  public void setCliTelefono(Integer cliTelefono)
  {
    this.cliTelefono = cliTelefono;
  }
  
  
  
  
 
  
}
