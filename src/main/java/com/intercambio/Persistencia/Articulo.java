/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Persistencia;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author familia
 */
@Entity
@Table(name="articulo")
public class Articulo {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer arIderegistro;
  private String arNombre;
  private Integer arTip;
  private String arEstado;
  private String arFoto;
  
  @JoinColumn(name="cli_idcliente")
  @ManyToOne
  private Cliente cliIdCliente;
  
  @OneToMany(mappedBy = "arIdArticuloRemitente")
  private transient List<Solicitud> soSolicitudRemitentes;
  
  @OneToMany(mappedBy = "arIdArticuloDestinatario")
  private transient List<Solicitud> soSolicitudDestinatarios;

  public Integer getArIderegistro()
  {
    return arIderegistro;
  }

  public void setArIderegistro(Integer arIderegistro)
  {
    this.arIderegistro = arIderegistro;
  }

  public String getArNombre()
  {
    return arNombre;
  }

  public void setArNombre(String arNombre)
  {
    this.arNombre = arNombre;
  }
  
  public String getArEstado()
  {
    return arEstado;
  }

  public void setArEstado(String arEstado)
  {
    this.arEstado = arEstado;
  }

  public String getArFoto()
  {
    return arFoto;
  }

  public void setArFoto(String arFoto)
  {
    this.arFoto = arFoto;
  }

  public Cliente getCliIdCliente()
  {
    return cliIdCliente;
  }

  public void setCliIdCliente(Cliente cliIdCliente)
  {
    this.cliIdCliente = cliIdCliente;
  }

  public Integer getArTip()
  {
    return arTip;
  }

  public void setArTip(Integer arTip)
  {
    this.arTip = arTip;
  }

  public List<Solicitud> getSoSolicitudRemitentes()
  {
    return soSolicitudRemitentes;
  }

  public void setSoSolicitudRemitentes(List<Solicitud> soSolicitudRemitentes)
  {
    this.soSolicitudRemitentes = soSolicitudRemitentes;
  }

  public List<Solicitud> getSoSolicitudDestinatarios()
  {
    return soSolicitudDestinatarios;
  }

  public void setSoSolicitudDestinatarios(List<Solicitud> soSolicitudDestinatarios)
  {
    this.soSolicitudDestinatarios = soSolicitudDestinatarios;
  }
  
  
  
  
}
