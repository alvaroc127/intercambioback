/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Persistencia.dao;

import com.intercambio.Persistencia.Articulo;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author familia
 */
public interface RepoInterfaceArticulo extends CrudRepository<Articulo, Integer> {
    
    Articulo findByArNombreIgnoreCase(final String nombreArticulo);
    
    @Query( value = "SELECT * FROM articulo WHERE cli_idcliente IS NOT NULL AND ar_estado= ?1",nativeQuery = true)
    List<Articulo> findByArEstado(final String estado);
  
}
