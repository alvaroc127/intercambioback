/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Persistencia.dao;


import com.intercambio.Persistencia.Solicitud;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author familia
 */
public interface RepoInterfaceSolicitud extends CrudRepository<Solicitud, Integer> {
  
  
  @Query(value = "SELECT sol.* FROM solicitud sol INNER JOIN articulo ar ON  sol.ar_idarticuloremitente = ar.ar_ideregistro "
 + "INNER JOIN cliente cl ON cl.cli_idregistro = ar.cli_idcliente "
 + "WHERE cl.cli_idregistro = ?1",nativeQuery = true)
  List<Solicitud> findAllNative(Integer idsolicitud);
}
