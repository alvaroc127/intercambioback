/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intercambio.Persistencia.dao;

import com.intercambio.Persistencia.Usuario;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author familia
 */
public interface RepoInterfaceUsuario  extends CrudRepository<Usuario, Integer>{

  Usuario findByUsCorreo(final String correo);
  
  Usuario findByUsCorreoAndUsPassword(final String correo,final String password);
  
}
